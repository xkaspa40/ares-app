
create database ares_data;

use ares_data;

CREATE USER 'ares_data'@'localhost' IDENTIFIED BY 'ares_data';

GRANT ALL PRIVILEGES ON ares_data.* TO 'ares_data'@'localhost';

CREATE TABLE `ares_data` (`ICO` varchar(8) NOT NULL PRIMARY KEY, 
`nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL, 
`sidlo` varchar(255) COLLATE utf8_czech_ci NOT NULL, 
`vznik` DATE NOT NULL, 
`platnost` DATE NOT NULL, 
`PSC` varchar(7) NOT NULL, 
`inserted` DATE NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

