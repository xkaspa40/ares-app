import React from "react";

class DataContent extends React.Component {
  constructor(props) {
    super(props);
    const data = props.data;
    this.state = {
      ICO: data.ICO,
      nazev: data.nazev,
      vznik: data.vznik,
      platnost: data.platnost,
      sidlo: data.sidlo,
      PSC: data.PSC,
      saved: data.saved,
    };
  }

  componentDidUpdate(prevProps) {
    if(this.props.data.ICO !== prevProps.data.ICO || this.props.data.saved !== prevProps.data.saved) 
    {
        const data = this.props.data;
        this.setState({
            ICO: data.ICO,
            nazev: data.nazev,
            vznik: data.vznik,
            platnost: data.platnost,
            sidlo: data.sidlo,
            PSC: data.PSC,
            saved: data.saved,
        });
    }
  } 

  onSave = (event) => {
    event.preventDefault();
    this.props.onSave(this.state);
  }

  getButton = () => {
      if (this.state.saved === false) {
          return <button type="button" className="btn btn-primary" onClick={this.onSave}>Uložit</button>;
      }
      return <button type="button" className="btn btn-success" disabled>Uloženo</button>
  }

  render() {
    return (
      <div className="bd">
        <p className="text-left h1">{this.state.nazev}</p>
        <p className="text-left">
          <span className="font-weight-bold">IČO: </span>
          {this.state.ICO}
        </p>
        <p className="text-left">
          <span className="font-weight-bold">Datum vzniku: </span>
          {this.state.vznik}
        </p>
        <p className="text-left">
          <span className="font-weight-bold">Platnost do: </span>
          {this.state.platnost}
        </p>
        <p className="text-left">
          <span className="font-weight-bold">Sídlo: </span>
          {this.state.sidlo}
        </p>
        <p className="text-left">
          <span className="font-weight-bold">PSČ: </span>
          {this.state.PSC}
        </p>
        {this.getButton()}
      </div>
    );
  }
}
export default DataContent;
