import React from "react";

class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      ICO: null,
      nazev: null,
      vznik: null,
      platnost: null,
      sidlo: null,
      psc: null,
    };
  }

  handleClick = (content) => {
    this.props.onSave(content);
  }

  makeTableBody = () => {
    return this.state.data.map((content) => {
      return (
        <tr key={content.ICO} onClick={(event) => this.handleClick(content)}>
          <td>{content.ICO}</td>
          <td>{content.nazev}</td>
          <td>{content.vznik}</td>
          <td>{content.platnost}</td>
          <td>{content.sidlo}</td>
          <td>{content.PSC}</td>
        </tr>
      );
    });
  };

  onSort = (event, key) => {
    const data = this.state.data;
    if (null === this.state[key] || this.state[key] === "desc") {
      data.sort((a, b) => a[key].localeCompare(b[key]));
      this.setState({ data });
      //reseting sorting state because of dipslaying sorting arrows
      this.setState({
        ICO: null,
        nazev: null,
        sidlo: null,
        platnost: null,
        vznik: null,
        PSC: null,
      });
      this.setState({ [key]: "asc" });
      return;
    }

    data.sort((a, b) => b[key].localeCompare(a[key]));
    this.setState({ data });
    this.setState({
      ICO: null,
      nazev: null,
      sidlo: null,
      platnost: null,
      vznik: null,
      PSC: null,
    });
    this.setState({ [key]: "desc" });

  };

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props.data) !== JSON.stringify(prevProps.data)) {
      const data = this.props.data;
      this.setState({
        data: data,
      });
    }
  }

  render() {
    return (
      <table className="table table-hover table-striped" style={{cursor: 'pointer'}}>
        <thead>
          <tr>
            <th scope="col" onClick={(e) => this.onSort(e, "ICO")}>
              IČO{" "}
              {this.state.ICO == null
                ? ""
                : this.state.ICO === "asc"
                ? "↑"
                : "↓"}
            </th>
            <th scope="col" onClick={(e) => this.onSort(e, "nazev")}>
              Název{" "}
              {this.state.nazev == null
                ? ""
                : this.state.nazev === "asc"
                ? "↑"
                : "↓"}
            </th>
            <th scope="col" onClick={(e) => this.onSort(e, "vznik")}>
              Datum vzniku{" "}
              {this.state.vznik == null
                ? ""
                : this.state.vznik === "asc"
                ? "↑"
                : "↓"}
            </th>
            <th scope="col" onClick={(e) => this.onSort(e, "platnost")}>
              Platnost do{" "}
              {this.state.platnost == null
                ? ""
                : this.state.platnost === "asc"
                ? "↑"
                : "↓"}
            </th>
            <th scope="col" onClick={(e) => this.onSort(e, "sidlo")}>
              Sídlo{" "}
              {this.state.sidlo == null
                ? ""
                : this.state.sidlo === "asc"
                ? "↑"
                : "↓"}
            </th>
            <th scope="col" onClick={(e) => this.onSort(e, "PSC")}>
              PSČ{" "}
              {this.state.PSC == null
                ? ""
                : this.state.PSC === "asc"
                ? "↑"
                : "↓"}
            </th>
          </tr>
        </thead>
        <tbody>{this.makeTableBody()}</tbody>
      </table>
    );
  }
}

export default DataTable;
