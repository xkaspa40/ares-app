import React from "react";
import InputGroup from "./InputGroup";
import DataContent from "./DataContent";
import DataTable from "./DataTable";
import ares from "../api/ares";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
    };
  }

  onSubmit = (type, value) => {
    if (type === "ico") {
      this.getByICO(value);
      return;
    }

    this.getByCompanyName(value);
  };

  getByICO = async (ico) => {
    const response = await ares.get("/byICO?ico=" + ico);
    this.setState({ data: response.data });
  };

  getByCompanyName = async (name) => {
    const response = await ares.get("/byCompanyName?companyName=" + name);
    this.setState({ data: response.data })
  };

  onSave = (data) => {
    this.saveCompany(data);
  }

  saveCompany = async (data) => {
    const response = await ares.post("/something", data);
    this.setState({ data: response.data });
  }

  getDataContentComponent = () => {
    if (this.state.data === null) return "";
    const length = this.state.data.length;
    if (length === 0) return <p className="h1">Žádný subjekt nenalezen.</p>;
    if (length === 1) return <DataContent data={this.state.data[0]} onSave={this.onSave} />;
    return <DataTable data={this.state.data} onSave={this.onSave} />;
  };

  render() {
    let data = this.getDataContentComponent;

    return (
      <div className="container" style={{ marginTop: "10vh" }}>
        <InputGroup onSubmit={this.onSubmit} />
        {data()}
      </div>
    );
  }
}

export default App;
