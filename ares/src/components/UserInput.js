import React from "react";

class UserInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      label: props.label,
      inputType: props.inputType,
      minLength: props.minLength,
      maxLength: props.maxLength,
      type: props.type,
      value: '',
      isInvalid: false,
      inputClass: "",
    };

  }

  onInputChange = (event) => {
    let text = event.target.value;
    if (!this.validateInput(text)) {
      this.setState({ inputClass: "is-invalid", isInvalid: true });
      return;
    }

    this.setState({ actLength: text.length, isInvalid: false, value: text, inputClass: "" });

    if (text.length >= this.state.minLength) {
      this.setState({ inputClass: "is-valid" });
      return;
    }
  }

  validateInput(input) {
    if (this.state.inputType === "number") {
      return input.match(/^\d+$/) || input.length === 0;
    }

    if (this.state.inputType === "alphanumeric") {
      return input.match(/^[\w * .]+$/) || input.length === 0;
    }

    return true;
  }

  onInputSubmit = (event) => {
    event.preventDefault();
    if (this.state.isInvalid) {
      alert("Nevalidní vstup!");
      return;
    }

    if (this.state.value.length < this.state.minLength) {
      alert("Minimální délka pro pole " + this.state.label + " je: " + this.state.minLength + " znaků!");
      return;
    }

    this.props.onSubmit(this.state.type, this.state.value);
  }

  render() {
    return (
      <div className="p-2 flex-fill">
        <form onSubmit={this.onInputSubmit}>
          <div className={"input-group mb-3 " + this.state.inputClass}>
            <div className="input-group-prepend">
              <span className="input-group-text" id="ico-append">
                {this.state.label}
              </span>
            </div>
            <input
              type="text"
              className={"form-control " + this.state.inputClass}
              onChange={this.onInputChange}
              placeholder={
                this.state.inputType === "number"
                  ? "Pouze číslice"
                  : "Alfanumerické znaky, mezera, *, ."
              }
              maxLength={this.state.maxLength}
            />
          </div>
        </form>
      </div>
    );
  }
}
export default UserInput;
