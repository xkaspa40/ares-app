import React from "react";
import UserInput from "./UserInput";

class InputGroup extends React.Component {

  render() {
    return (
      <div className="d-flex flex-row row">
        <UserInput label="IČO" inputType="number" minLength={8} maxLength={8} onSubmit={this.props.onSubmit} type="ico" />
        <UserInput label="Název firmy" inputType="alphanumeric" minLength={3} onSubmit={this.props.onSubmit} type="name" />
      </div>
    );
  }
}
export default InputGroup;
