<?php
require_once '../vendor/restler.php';
use Luracast\Restler\Restler;
use Luracast\Restler\Defaults;


Defaults::$crossOriginResourceSharing = true;
Defaults::$accessControlAllowOrigin = '*';
Defaults::$accessControlAllowMethods = 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD';


$r = new Restler();
$r->addAPIClass('AresMiner'); // repeat for more
$r->handle(); //serve the response
