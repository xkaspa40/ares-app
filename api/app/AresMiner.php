<?php

class AresMiner
{

    /**
     * @var string
     */
    private $address = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi?';

    /*
     * @var resource
     */
    private $context = null;

    /**
     * @var string
     */
    private $host = "localhost"; 

    /**
     * @var string
     */
    private $user = "ares_data"; 

    /**
     * @var string
     */
    private $password = 'ares_data'; 

    /**
     * @var string
     */
    private $dbname = "ares_data"; 

    /**
     * GET ico 
     * First we try DB, then send request to ARES
     *
     * @param string $ico
     * @return array
     */
    public function byICO(string $ico = '29187206') : array
    {
        $data = $this->getFromDb($ico, null); 
        if ($data !== []) {
            return $data;
        }
        $stringXml = $this->getRemoteContents("ico=".$ico);
        return $this->handleResult($stringXml);
    }

    /**
     * GET companyName
     * First we try DB, then send request to ARES
     *
     * @param string $companyName
     * @return array
     */
    public function byCompanyName(string $companyName = "ACMARK") : array
    {
        $data = $this->getFromDb(null, $companyName); 
        if ($data !== []) {
            return $data;
        }
        $stringXml = $this->getRemoteContents("obchodni_firma=". urlencode($companyName));
        return $this->handleResult($stringXml);
    }

    /**
     * POST request
     *
     * @return array
     */
    public function postSomething() : array
    {
        $connection = mysqli_connect($this->host, $this->user, $this->password, $this->dbname);

        if (! $connection) {
            return false;
        }
     
        return $this->saveInDb($this->restler->getRequestData(), $connection);
    }


    /**
     * Gets record from DB. Uses strict comparisons (no LIKE %...%) 
     * so it never will return more than one record. 
     *
     * @param string|null $ico
     * @param string|null $companyName
     * @return array
     */
    private function getFromDb(?string $ico, ?string $companyName) : array
    {
        $connection = mysqli_connect($this->host, $this->user, $this->password, $this->dbname);
        $field = null === $ico ? "nazev" : "ico";
        $value = null === $ico ? $companyName : $ico;
        
        // using old techique of escape_string() because prepared statements didn't work 
        // for me in this case for some reason...
        $field = mysqli_real_escape_string($connection, $field);
        $value = mysqli_real_escape_string($connection, $value);
        $data = $connection->query("SELECT * FROM ares_data WHERE $field = " . "'" . "$value" . "'" . "AND `inserted` + INTERVAL 30 DAY > NOW()");
        $data = mysqli_fetch_assoc($data);
        if ( ! $data) {
            return [];
        }

        $outer = array();
        $data['saved'] = true;
        $outer[] = $data;
        return $outer;
    }

    /**
     * Saves company info into database with timestamp.
     *
     * @param array $data
     * @param resource $connection
     * @return array
     */
    private function saveInDb(array $data, $connection) : array
    {
        $res = $connection->prepare("INSERT INTO ares_data (ICO, nazev, sidlo, vznik, platnost, PSC, inserted) 
        VALUES(?, ?, ?, ?, ?, ?, NOW()) ON DUPLICATE KEY UPDATE
        `ICO`=VALUES(`ICO`), `nazev`=VALUES(`nazev`), `sidlo`=VALUES(`sidlo`), `vznik`=VALUES(`vznik`),
        `platnost`=VALUES(`platnost`), `PSC`=VALUES(`PSC`), `inserted`=NOW()");

        $res->bind_param("ssssss", $data['ICO'], $data['nazev'], $data['sidlo'], $data['vznik'], $data['platnost'], $data['PSC']);
        if ( ! $res->execute()) {
            return [];
        }
        
        $data['saved'] = true;
        unset($data['request_data']);
        $outer = array();
        $outer[] = $data;
        return $outer;
    }


    /**
     * Parses xml result so it's easier to put it in array
     *
     * @param string|null $stringXml
     * @return array
     */
    private function handleResult(?string $stringXml): array
    {
        $result = simplexml_load_string($stringXml)->children('are', true);
        if ($result->Odpoved->Pocet_zaznamu->__toString() === '0') {
            return [];
        }
        return $this->fillArray($result);
    }

    /**
     * Creates an array which is then automaticaly converted to JSON 
     * and sent as a response
     *
     * @param Object $result
     * @return array
     */
    private function fillArray(Object $result): array 
    {
        $response = array();
        $innner = array();
        foreach ($result->Odpoved->Zaznam as $zaznam) {
            $inner['nazev'] = $zaznam->Obchodni_firma->__toString();
            $inner['ICO'] = $zaznam->ICO->__toString();
            $inner['vznik'] = $zaznam->Datum_vzniku->__toString();
            $inner['platnost'] = $zaznam->Datum_platnosti->__toString();
            $address = $zaznam->Identifikace->Adresa_ARES->children('dtt', true);
            $inner['sidlo'] = "$address->Nazev_ulice $address->Cislo_domovni, $address->Nazev_obce";
            $inner['PSC'] = $address->PSC->__toString();
            // used for generating save button on frontend
            $inner['saved'] = false;
            $response[] = $inner;
        }

        return $response;
    }

    /**
     * Gets data from ARES
     *
     * @param string $suffix
     * @return void
     */
    private function getRemoteContents(string $suffix)
    {
        $this->createContext();
        return file_get_contents($this->address.$suffix, false, $this->context);
    }

    /**
     * Creates stream for ARES service
     *  
     * @return void
     */
    private function createContext(): void
    {
       $this->context = stream_context_create(['http' => ['timeout' => 1000]]);
    }
}