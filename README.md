## How to run

Everything is gonna be run on localhost.<br /><br />
Log in as a root to your local mysql server. <br/>
Copy contents of sql_create.sql and paste it inside. <br/>
Go to /ares-app/ares/ and run `npm install` <br/>
Run `yarn start` <br /><br />
Open new terminal window. <br />
Go to /ares-app/api and run `composer install` <br />
Go to /ares/app/api/public and run `php -S localhost:8888` <br /><br />
Project should be available at localhost:3000 with api running at localhost:8888
